# Blank Inference Service

Blank inference service, as template and/or for testing.

Return random numbers as predictions without any actual model inferencing. 

## Running Inference Services

All models are running in the namespace `admon-dev`. 

To call them: 
* POST to url: http://ml.cern.ch/v1/models/<model-name>:predict
* Header: 
  * Host: `<model-name>-transformer-default.admon-dev.example.com` or `<model-name>-transformer-default.admon-dev.svc.cluster.local` for local calls(local doesnt require authservice_session cookie)
  * Cookie: `authservice_session=...` Cookie from http://ml.cern.ch
* Body: `{"instances":[<Instances>]}`

Response: `{"predictions":[<Predictions>]}`  

### Complete Models

Return same amount of Predictions as they receive Instances.
* `blank-model`: Returns result in 1-2 seconds
* `blank-model-long`: Returns result in 30-60 seconds
* `blank-model-very-long`: Returns result in 4-6 minutes (Currently receives Gateway timeout)


### Incomplete Models

Returns incomplete amount of Predictions for Instances.
* `blank-model-incomplete`: Each instance has 80% to receive a prediction. Returns result in 1-2 seconds.
