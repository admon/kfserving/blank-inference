import json
import time
import random
import logging
import kfserving
from typing import *

logging.basicConfig(level=kfserving.constants.KFSERVING_LOGLEVEL)


def preprocess(instance):
    # Create Featurevector for each instance
    return random.sample(range(10), k=5)


class Transformer(kfserving.KFModel):
    def __init__(self, name: str, predictor_host: str):
        super().__init__(name)
        self.predictor_host = predictor_host
        logging.info(f"MODEL NAME {name}")
        logging.info(f"PREDICTOR URL {self.predictor_host}")

    def preprocess(self, inputs: Dict[str, List]) -> Dict[str, List]:
        logging.info("Inputs length: " + str(len(inputs["instances"])))
        preprocessed_data = list(map(preprocess, inputs["instances"]))
        logging.info("Preprocess-Transformation successfull, calling model:predict")
        return { "instances": preprocessed_data }

    async def predict(self, request: Dict[str, List]) -> Dict[str, List]:
        # send request to predict model with
        # response = await self._http_client.fetch(...)

        # Simulate prediction, reduce feature vector to a single value and wait random amount of time
        time.sleep(1 + random.random())
        return { "predictions": list(map(lambda inst: inst[0], request["instances"])) }

    def postprocess(self, inputs: List) -> List:
        logging.info("sent: " + str(inputs))
        return inputs

